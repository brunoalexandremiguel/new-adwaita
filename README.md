# New Adwaita

**This is a fork of the New Adwaita GTK theme being developed by Matthias Clasen**, made available here: https://download.gnome.org/misc/testing/Adwaita

### Changes from the original source
- CSD height decreased/optimized for a 1366x768 screen resolution
- Decreased height in Gnome Terminal tabs
- Decreased some buttons' padding

![new adwaita](https://i.imgur.com/4W5O44F.png)